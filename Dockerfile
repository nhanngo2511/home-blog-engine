FROM mcr.microsoft.com/dotnet/core/aspnet:2.2-stretch-slim AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/core/sdk:2.2-stretch AS build
WORKDIR /src
COPY ["BlogEngine.sln", "./"]
COPY ["BE.Web/BE.Web.csproj", "BE.Web/"]
COPY ["BE.Repo/BE.Repo.csproj", "BE.Repo/"]
COPY ["BE.Service/BE.Service.csproj", "BE.Service/"]
# COPY ["BE.UnitTest/BE.UnitTest.csproj", "BE.UnitTest/"]

# RUN dotnet restore
# COPY . .
# WORKDIR "/src/BE.Repo"
# RUN dotnet build -c Release -o /app

# RUN dotnet restore
# COPY . .
# WORKDIR "/src/BE.UnitTest"
# RUN dotnet build -c Release -o /app


# RUN dotnet restore
# COPY . .
# WORKDIR "/src/BE.Service"
# RUN dotnet build -c Release -o /app

RUN dotnet restore "BE.Repo/BE.Repo.csproj"
RUN dotnet restore "BE.Service/BE.Service.csproj"

RUN dotnet restore "BE.Web/BE.Web.csproj"
COPY . .
WORKDIR "/src/BE.Web"
RUN dotnet build "BE.Web.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish -c Release -o /app/publish

# RUN dotnet ef database update

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "BE.Web.dll"]