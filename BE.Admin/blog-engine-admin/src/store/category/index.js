import CategoryService from '@/services/category-service'

const state = {
    categories: [],
    searchedCategories: [],
    loading: false,
    pagination: {
        rowsPerPage: 10,
        descending: false,
        page: 1,
        sortBy: "CreatedDate",
    },
    totalCategories: 0,
    selectedCategoryId: 0,
    categoryPost:{},
    isDeleted: false,
    isCreated: false,
    isEdited: false
};

const getters = {
    categories: state => state.categories,
    searchedCategories : state => state.searchedCategories,
    loading: state => state.loading,
    totalCategories: state => state.totalCategories,
};

const mutations = {
    SetCategories(state, data){
        state.categories = data;
    },
    SetSearchedCategories(state, data){
        state.searchedCategories = data;
    },
    SearchStart(state) {
        state.loading = true;
    },
    SearchEnd(state) {
        state.loading = false;
    }, 
    AssignPagination(state, payload){
        state.pagination.page = payload.page;
        state.pagination.rowsPerPage = payload.itemsPerPage;
        state.pagination.sortBy = payload.sortBy.find(e => !!e);
        state.pagination.descending = payload.sortDesc[0];
    },
    SetTotalCategories(state, data){
        state.totalCategories = data;
    },
    SetSelectedCategoryId(state, data){
        state.selectedCategoryId = data;
    },
    SetCategoryPost(state, data){
        state.categoryPost = data;
    },
    SetIsDeleted(state, payload){
        state.isDeleted = payload;
    },
    SetIsCreated(state, payload){
        state.isCreated = payload;
    },
    SetIsEdited(state, payload){
        state.isEdited = payload;
    }
};

const actions = {
    GetCategories({commit}){
        let token = this.getters['account/token'];
        return CategoryService.getAll(token)
            .then((response) => {
                commit("SetCategories", response.data);
            })
            .catch(error => {
                throw new Error(error);
            });
    },
    SearchCategoies({commit, state}){
        let token = this.getters['account/token'];
        commit("SearchStart");

        var searchParam = state.pagination;

        return CategoryService.search(searchParam, token)
            .then((response) => {
                commit("SetSearchedCategories", response.data.categoies);
                commit("SearchEnd");
                commit("SetTotalCategories", response.data.totalCategories);
            })
            .catch(error => {
                commit("SearchEnd");
                throw new Error(error);
            });
    },

    CreateCategory({commit, state}){
        let token = this.getters['account/token'];
        let param = state.categoryPost;

        return CategoryService.create(param, token)
        .then((response) => {
            let isNotNullReponse = (response.data != null);
            if(isNotNullReponse){
                commit("SetIsCreated", isNotNullReponse); 
                this.dispatch('category/SearchCategoies')
            }
        })
        .catch(error => {
            throw new Error(error);
        });
    },

    EditCategory({commit, state}){
        let token = this.getters['account/token'];
        let param = state.categoryPost;

        return CategoryService.update(param, token)
        .then((response) => {
            let isNotNullReponse = (response.data != null);
            if(isNotNullReponse){
                commit("SetIsCreated", isNotNullReponse); 
                this.dispatch('category/SearchCategoies')
            }
        })
        .catch(error => {
            throw new Error(error);
        });
    },
    DeleteCatergory({commit, state}){
        let token = this.getters['account/token'];
        var id = state.selectedCategoryId;
        
        return CategoryService.delete(id, token)
        .then((response) => {
            let isNotNullReponse = (response.data != null);
            if(isNotNullReponse){
                commit("SetIsCreated", isNotNullReponse); 
                this.dispatch('category/SearchCategoies')
            }         
        })
        .catch(error => {
            throw new Error(error);
        });
    },
};

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}