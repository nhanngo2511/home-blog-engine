import BlogService from '@/services/blog-service'

const state = {
    blogs: [],
    loading: true,
    totalBlogs: 0,
    pagination: {
        rowsPerPage: 10,
        descending: true,
        page: 1,
        sortBy: "CreatedDate",
    },
    blogPost:{},
    selectedBlogId: 0,
    isDeleted: false,
    isCreated: false,
    isEdited: false
    
};

const getters = {
    blogs: state => state.blogs,
    loading: state => state.loading,
    totalBlogs: state => state.totalBlogs,
    isDeleted: state => state.isDeleted,
    isCreated: state => state.isCreated,
    isEdited: state => state.isEdited
};

const mutations = {
    SearchStart(state) {
        state.loading = true;
    },
    SearchEnd(state) {
        state.loading = false;
    },    
    SetBlogs(state, data){
        state.blogs = data;
    },
    SetTotalBlogs(state, data){
        state.totalBlogs = data;
    },
    AssignPagination(state, payload){
        state.pagination.page = payload.page;
        state.pagination.rowsPerPage = payload.itemsPerPage;
        state.pagination.sortBy = payload.sortBy.find(e => !!e);
        state.pagination.descending = payload.sortDesc.find(e => !!e);

    },
    SetBlogPost(state, payload){
        state.blogPost = payload;
    },
    SetSelectedBlogId(state, payload){
        state.selectedBlogId = payload;
    },
    SetIsDeleted(state, payload){
        state.isDeleted = payload;
    },
    SetIsCreated(state, payload){
        state.isCreated = payload;
    },
    SetIsEdited(state, payload){
        state.isEdited = payload;
    }
};

const actions = {
    SearchBlogs({commit, state}){
        
        let token = this.getters['account/token'];

        commit("SearchStart");

        var searchParam = state.pagination;

        return BlogService.post(searchParam, token)
            .then((response) => {
                commit("SetBlogs", response.data.blogs);
                commit("SearchEnd");
                commit("SetTotalBlogs", response.data.totalBlogs);
            })
            .catch(error => {
                commit("SearchEnd");
                throw new Error(error);
            });
    },

    CreateBlog({commit, state}){
        let token = this.getters['account/token'];

        let param = new FormData();
        param.append('file', state.blogPost.file);
        param.append('content', state.blogPost.content);
        param.append('title', state.blogPost.title);
        param.append('categoryId', state.blogPost.categoryId);
        param.append('thumnailImageName', state.blogPost.thumnailImageName);
        
        return BlogService.create(param, token)
        .then((response) => {
            let isNotNullReponse = (response.data != null);

            if(isNotNullReponse){
                commit("SetIsCreated", isNotNullReponse); 
                this.dispatch('blog/SearchBlogs');
            }
            
        })
        .catch(error => {
            throw new Error(error);
        });
    },

    EditBlog({commit, state}){
        let token = this.getters['account/token'];

        let param = new FormData();
        param.append('id', state.blogPost.id);
        param.append('file', state.blogPost.file);
        param.append('content', state.blogPost.content);
        param.append('title', state.blogPost.title);
        param.append('categoryId', state.blogPost.categoryId);
        param.append('thumnailImageName', state.blogPost.thumnailImageName);
        
        return BlogService.edit(param, token)
        .then((response) => {
            let isNotNullReponse = (response.data != null);
            commit("SetIsEdited", isNotNullReponse); 
            this.dispatch('blog/SearchBlogs');

        })
        .catch(error => {
            throw new Error(error);
        });
    },

    DeleteBlog({commit, state}){
        let token = this.getters['account/token'];

        var id = state.selectedBlogId;
        return BlogService.delete(id, token)
        .then((response) => {
            commit("SetIsDeleted", response.data);   
            this.dispatch('blog/SearchBlogs');
        })
        .catch(error => {
            throw new Error(error);
        });
    },

};

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}