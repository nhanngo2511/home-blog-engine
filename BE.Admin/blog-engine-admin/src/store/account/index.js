import AccountService from '@/services/account-service'

const state = {
   username: '',
   password: '',
   token: localStorage.getItem('token') || '',
   isAuthenticated: false,
   isInvalidUser: true
};

const getters = {
    token: state => state.token,
    isInvalidUser: state => state.isInvalidUser,
    isAuthenticated: state => state.isAuthenticated,
};

const mutations = {
    SetLoginModel(state, payload){
        state.username = payload.username;
        state.password = payload.password;
    },
    SetAuthor(state, token){
        state.token = token
        state.isAuthenticated = true;
    },

    SetLogout(state){
        state.token = '';
        state.isAuthenticated = false;
    },

    SetIsAuthenticated(payload){
        state.isAuthenticated = payload;
    },

    SetInvalidUser(){
        state.isInvalidUser = true;
    },

    SetValidUser(){
        state.isInvalidUser = false;
    },
};

const actions = {
    Login({commit, state}){

        var loginModel = {
            username: state.username,
            password: state.password
        }

       return AccountService.login(loginModel)
            .then((response) => {
                var token = response.data.token;
                if(token == undefined){
                    commit('SetLogout');
                    commit('SetValidUser');

                }else{
                    localStorage.setItem('token', token);
                    commit('SetAuthor', token);
                    commit('SetInvalidUser');

                }               
            })
            .catch(error => {
                throw new Error(error);
        });

    },

    Logout({commit}){
       commit('SetLogout');
       localStorage.removeItem('token');
    },
};

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}