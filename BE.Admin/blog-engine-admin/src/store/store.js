import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex)

import account from "./account";
import blog from "./blog";
import category from "./category";
import comment from "./comment";

export default new Vuex.Store({
    state: {},
    mutations: {},
    getters: {},
    actions: {},
    modules: {
        account,
        blog,
        category,
        comment
    }
  });