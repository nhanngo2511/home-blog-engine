import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import Vuex from 'vuex';
import router from './router.js';
import store from './store/store';
import ApiService from './services/api-service';

Vue.config.productionTip = false

ApiService.init();

new Vue({
  vuetify,
  Vuex,
  router,
  store,
  render: h => h(App)
}).$mount('#app')
