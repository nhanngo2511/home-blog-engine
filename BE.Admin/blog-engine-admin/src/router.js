import Vue from 'vue';
import Router from 'vue-router';
import Blog from './components/Blog.vue';
import Category from './components/Category.vue';
import Login from './components/Login.vue';


Vue.use(Router);

const routes = new Router({
   
    routes: [
      {
        path: '/blog',
        name: 'blog',
        component: Blog
      },
      {
        path: '/category',
        name: 'category',
        component: Category
      },
      {
        path: '/login',
        name: 'login',
        component: Login
      }
    ]
  });

  routes.beforeEach((to, from, next) => {
  //   redirect to login page if not logged in and trying to access a restricted page
    const publicPages = ['/login'];
    const authRequired = !publicPages.includes(to.path);
    const loggedIn = localStorage.getItem('token');
  
    if (authRequired && !loggedIn) {
      return next('/login');
    }
  
    next();
  });

  export default routes;