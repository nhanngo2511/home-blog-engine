import axios from 'axios'

const API_URL = 'http://localhost:5001/api';

const ApiService = {
    init() {
        // let token = localStorage.getItem('token') || '';

        axios.defaults.baseURL = API_URL;
        // axios.defaults.headers.common['Authorization'] ='Bearer ' + token;
    },

    query(resource, params, token) {
        return axios.get(resource, params, {
            headers:{
                'Authorization': 'Bearer ' + token
            }
        }).catch(error => {
            throw new Error(`BlogService ${error}`);
        });
    },

    get(resource, Id, token) {
        return axios.get(`${resource}/${Id}`, {
            headers:{
                'Authorization': 'Bearer ' + token
            }
        }).catch(error => {
            throw new Error(`BlogService ${error}`);
        });
    },

    post(resource, data, token) {
        return axios.post(resource, data, {
            headers:{
                'Authorization': 'Bearer ' + token
            }
        }).catch(error => {
            throw new Error(`BlogService ${error}`);
        });
    },

    getAll(resource, token) {
        return axios.get(`${resource}`, {
            headers:{
                'Authorization': 'Bearer ' + token
            }
        }).catch(error => {
            throw new Error(`BlogService ${error}`);
        });
    },

    postHaveIncludeFile(resource, data, token) {
        return axios.post(resource, data, {
            headers: {
                'Content-Type': 'multipart/form-data',
                'Authorization': 'Bearer ' + token
            }
        }).catch(error => {
            throw new Error(`BlogService ${error}`);
        });
    },
    
}

export default ApiService;