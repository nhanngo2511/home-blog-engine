import ApiService from './api-service';

const CategoryService = {
    getAll(token){
        return ApiService.getAll("category/categories", token);
    },

    search(data, token){
        return ApiService.post("category/search", data, token);
    },

    create(data, token){
        return ApiService.post("category/create", data, token); 
    },

    update(data, token){
        return ApiService.post("category/update", data, token); 
    },

    delete(data, token) {
        return ApiService.get("category/delete", data, token);
    }
};

export default CategoryService;