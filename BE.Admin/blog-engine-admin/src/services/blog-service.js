import ApiService from './api-service';

const BlogService = {
    query(params, token) {
        return ApiService.query("blog/", {
            params: params
        }, token);
    },
    get(slug, token) {
        return ApiService.get("blog/", slug, token);
    },
    post(data, token) {
        return ApiService.post("blog/search", data, token);
    },

    create(data, token) {
        return ApiService.postHaveIncludeFile("blog/create", data, token);
    },

    edit(data, token) {
        return ApiService.postHaveIncludeFile("blog/update", data, token);
    },

    delete(data, token) {
        return ApiService.get("blog/delete", data, token);
    }
};

export default BlogService;