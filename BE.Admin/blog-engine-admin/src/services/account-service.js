import ApiService from './api-service';

const AccountService = {

    login(data) {
        return ApiService.post("account/login", data);
    },

};

export default AccountService;