﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE.Repo;
using BE.Repo.Repository;

namespace BE.Service
{
    public class BlogService : IBlogService
    {
        private readonly IBlogRepository _blogRepository;

        public BlogService(IBlogRepository blogRepository)
        {
            this._blogRepository = blogRepository;
        }

        public async Task<Blog> CreateBlog(Blog blog)
        {
            return await _blogRepository.CreateBlog(blog);
        }

        public async Task DeleteBlog(int blogId)
        {
            await _blogRepository.DeleteBlog(blogId);
        }

        public async Task<Blog> GetBlog(int id)
        {
           return await _blogRepository.GetBlog(id);
        }

        public async Task<IEnumerable<Blog>> GetBlogByCategoryId(int categoryId)
        {
            return await _blogRepository.GetBlogsByCategoryId(categoryId);
        }

        public async Task<IEnumerable<Blog>> GetBlogs()
        {
            return await _blogRepository.GetBlogs();
        }

        public async Task<IEnumerable<Blog>> GetLatestBlogs()
        {
            var blogsResult = await _blogRepository.GetBlogs();
            blogsResult = blogsResult.OrderByDescending(x => x.CreatedDate);

            return blogsResult;
        }

        public async Task<int> GetTotalBlogs()
        {
            return await _blogRepository.GetTotalBlogs();
        }

        public async Task<IEnumerable<Blog>> RelatedBlogs(int currentBlogId, int categoryId)
        {
            return await _blogRepository.RelatedBlogs(currentBlogId, categoryId);
        }

        public async Task<Blog> UpdateBlog(Blog blog)
        {
            return await _blogRepository.UpdateBlog(blog);
        }
    }
}
