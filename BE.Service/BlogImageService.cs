﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BE.Repo;
using BE.Repo.Repository;

namespace BE.Service
{
    public class BlogImageService : IBlogImageService
    {
        private readonly IBlogImageRepository _blogImageRepository;

        public BlogImageService(IBlogImageRepository blogImageRepository)
        {
            this._blogImageRepository = blogImageRepository;
        }

        public async Task AssignImageToBlog(List<string> imageNames, int blogId)
        {
            await _blogImageRepository.AssignImageToBlog(imageNames, blogId);
        }

        public async Task CreateBlogImage(string imageName)
        {
            await _blogImageRepository.CreateBlogImage(imageName);
        }

        public async Task DeleteImagesNotInBlog(List<string> imageNames)
        {
            await _blogImageRepository.DeleteImagesNotInBlog(imageNames);
        }

        public async Task<IEnumerable<BlogImage>> GetImageByBlogID(int blogId)
        {
            return await _blogImageRepository.GetImageByBlogID(blogId);
        }

        public async Task<List<string>> GetImageNameNotInBlog()
        {
            return await _blogImageRepository.GetImageNameNotInBlog();
        }
    }
}
