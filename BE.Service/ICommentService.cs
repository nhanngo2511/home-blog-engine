﻿using BE.Repo;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BE.Service
{
    public interface ICommentService
    {
        Task<IEnumerable<Comment>> GetCommentByBlogId(int BlogId);
        Task<Comment> CreateComment(Comment comment);
        Task<Comment> GetCommentByID(int Id);
    }
}
