﻿using BE.Repo;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BE.Service
{
    public interface ICategoryService
    {
        Task<IEnumerable<Category>> GetCategories();
        Task<Category> CreateCategory(Category category);
        Task<Category> UpdateCategory(Category category);
        Task DeleteCategory(int categoryId);

        Task<Category> GetCategoryById(int categoryId);

        Task<IEnumerable<Category>> GetLatestCategories();
    }
}
