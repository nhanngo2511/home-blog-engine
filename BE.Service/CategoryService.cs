﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE.Repo;
using BE.Repo.Repository;

namespace BE.Service
{
    public class CategoryService : ICategoryService
    {
        private readonly ICategoryRepository _categoryRepository;

        public CategoryService(ICategoryRepository categoryRepository)
        {
            this._categoryRepository = categoryRepository;
        }

        public async Task<Category> CreateCategory(Category category)
        {
            return await _categoryRepository.CreateCategory(category);
        }

        public async Task DeleteCategory(int categoryId)
        {
            await _categoryRepository.DeleteCategory(categoryId);
        }

        public async Task<IEnumerable<Category>> GetCategories()
        {
            return await _categoryRepository.GetCategories();
        }

        public async Task<Category> GetCategoryById(int categoryId)
        {
            return await _categoryRepository.GetCategoryById(categoryId);
        }

        public async Task<IEnumerable<Category>> GetLatestCategories()
        {
            var categoriesResult = await _categoryRepository.GetCategories();
            categoriesResult = categoriesResult.OrderByDescending(x => x.CreatedDate);

            return categoriesResult;
        }

        public async Task<Category> UpdateCategory(Category category)
        {
           
            return await _categoryRepository.UpdateCategory(category);
        }
    }
}
