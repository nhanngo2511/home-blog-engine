﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BE.Repo;
using BE.Repo.Repository;

namespace BE.Service
{
    public class CommentService : ICommentService
    {
        private readonly ICommentRepository _commentRepository;

        public CommentService(ICommentRepository commentRepository)
        {
            this._commentRepository = commentRepository;
        }

        public async Task<Comment> CreateComment(Comment comment)
        {
            return await _commentRepository.CreateComment(comment);
        }

        public async Task<IEnumerable<Comment>> GetCommentByBlogId(int BlogId)
        {
            return await _commentRepository.GetCommentsByBlogId(BlogId);
        }

        public async Task<Comment> GetCommentByID(int Id)
        {
            return await _commentRepository.GetCommentByID(Id);
        }
    }
}
