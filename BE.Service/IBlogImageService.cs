﻿using BE.Repo;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BE.Service
{
    public interface IBlogImageService
    {
        Task<IEnumerable<BlogImage>> GetImageByBlogID(int blogId);
        Task CreateBlogImage(string imageName);
        Task AssignImageToBlog(List<string> imageNames, int blogId);
        Task DeleteImagesNotInBlog(List<string> imageNames);
        Task<List<string>> GetImageNameNotInBlog();
    }
}
