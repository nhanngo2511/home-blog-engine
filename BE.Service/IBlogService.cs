﻿using BE.Repo;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BE.Service
{
    public interface IBlogService
    {
        Task<IEnumerable<Blog>> GetBlogs();
        Task<Blog> GetBlog(int id);
        Task<Blog> CreateBlog(Blog blog);
        Task<Blog> UpdateBlog(Blog blog);
        Task DeleteBlog(int blogId);
        Task<IEnumerable<Blog>> RelatedBlogs(int currentBlogId, int categoryId);
        Task<IEnumerable<Blog>> GetBlogByCategoryId(int categoryId);
        Task<int> GetTotalBlogs();
        Task<IEnumerable<Blog>> GetLatestBlogs();
    }
}
