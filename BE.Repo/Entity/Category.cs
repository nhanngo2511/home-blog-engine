﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BE.Repo
{
    public class Category : BaseEntity
    {
        public string Content { get; set; }

        public IEnumerable<Blog> Blogs { get; set; }
    }
}
