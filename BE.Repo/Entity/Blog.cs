﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BE.Repo
{
    public class Blog : BaseEntity
    {
        public string Title { get; set; }
        public string ThumnailImageName { get; set; }
        public string Content { get; set; }
        public int CategoryId { get; set; }
        public virtual Category Category { get; set; }
        public IEnumerable<Comment> Commnents { get; set; }
        public IEnumerable<BlogImage> BlogImages { get; set; }
    }
}
