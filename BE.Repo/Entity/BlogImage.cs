﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BE.Repo
{
    public class BlogImage : BaseEntity
    {
        public string FileName { get; set; }
        public int? BlogId { get; set; }

        public virtual Blog Blog { get; set; }
    }
}
