﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BE.Repo
{
    public class Comment : BaseEntity
    {
        public string Content { get; set; }
        public int BlogId { get; set; }

        public virtual Blog Blog { get; set; }
    }
}
