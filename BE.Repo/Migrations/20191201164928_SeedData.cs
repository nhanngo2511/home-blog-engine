﻿using Microsoft.EntityFrameworkCore.Migrations;
using System.IO;

namespace BE.Repo.Migrations
{
    public partial class SeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var sqlPath = Path.Combine("../BE.Repo/SeedData/GenerateFirstData.sql");
            migrationBuilder.Sql(File.ReadAllText(sqlPath));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
