﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace BE.Repo.Repository
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly ApplicationContext _context;
        private readonly DbSet<Category> _categoryEntity;

        public CategoryRepository(ApplicationContext context)
        {
            this._context = context;
            this._categoryEntity = context.Set<Category>();
        }

        public async Task<Category> CreateCategory(Category category)
        {
            await _context.AddAsync(category);

            await _context.SaveChangesAsync();

            var categoryResult = await GetCategory(category.Id);

            return categoryResult;
        }

        public async Task DeleteCategory(int categoryId)
        {
            var category = await _categoryEntity.FindAsync(categoryId);

            _context.Remove(category);

            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Category>> GetCategories()
        {
            return await _categoryEntity.Include(x => x.Blogs).ToListAsync();
        }

        public async Task<Category> GetCategoryById(int categoryId)
        {
            return await _categoryEntity.Include(x => x.Blogs).FirstOrDefaultAsync(x => x.Id == categoryId);
        }

        public async Task<Category> UpdateCategory(Category category)
        {
            _categoryEntity.Update(category);

            await _context.SaveChangesAsync();

            return await GetCategory(category.Id);
        }

        private async Task<Category> GetCategory(int categoryId) {
            return await _categoryEntity.FindAsync(categoryId);
        }
    }
}
