﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BE.Repo.Repository
{
    public interface ICategoryRepository
    {
        Task<IEnumerable<Category>> GetCategories();
        Task<Category> CreateCategory(Category category);
        Task<Category> UpdateCategory(Category category);
        Task DeleteCategory(int categoryId);
        Task<Category> GetCategoryById(int categoryId);
    }
}
