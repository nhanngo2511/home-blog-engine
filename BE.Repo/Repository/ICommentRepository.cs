﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BE.Repo.Repository
{
    public interface ICommentRepository
    {
        Task<IEnumerable<Comment>> GetCommentsByBlogId(int BlogID);
        Task<Comment> CreateComment(Comment comment);
        Task<Comment> GetCommentByID(int Id);
    }
}
