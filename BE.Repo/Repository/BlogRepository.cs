﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE.Repo.Repository
{
    public class BlogRepository : IBlogRepository
    {
        private readonly ApplicationContext _context;
        private readonly DbSet<Blog> _blogEntity;

        public BlogRepository(ApplicationContext context)
        {
            this._context = context;
            _blogEntity = context.Set<Blog>();
        }

        public async Task<Blog> CreateBlog(Blog blog)
        {
            await _context.AddAsync(blog);
            
            var blogIdResult = await _context.SaveChangesAsync();

            var blogResult = await GetBlog(blogIdResult);

            return blogResult;
        }

        public async Task DeleteBlog(int blogId)
        {
            var blog = await _blogEntity.FindAsync(blogId);

            _context.Remove(blog);

            await _context.SaveChangesAsync();
        }

        public async Task<Blog> GetBlog(int id)
        {
            return await _blogEntity.Include(a => a.Category).Include(a => a.Commnents).AsNoTracking().SingleOrDefaultAsync(a => a.Id == id);
        }

        public async Task<IEnumerable<Blog>> GetBlogsByCategoryId(int categoryId)
        {
            var result = await _blogEntity.Include(a => a.Category).Include(a => a.Commnents).AsNoTracking().Where(a => a.CategoryId == categoryId).ToListAsync();

            return result;
        }
        public async Task<IEnumerable<Blog>> GetBlogs()
        {
            var result = await _blogEntity.Include(b => b.Category).Include(b => b.Commnents).AsNoTracking().ToListAsync();

            return result;
        }

        public async Task<int> GetTotalBlogs()
        {
            return await _blogEntity.CountAsync();
        }

        public async Task<IEnumerable<Blog>> RelatedBlogs(int currentBlogId, int categoryId)
        {
            return await _blogEntity.Where(b => b.Id != currentBlogId && b.Category.Id == categoryId)
                .Include(b => b.Category)
                .Include(b => b.Commnents)
                .AsNoTracking()
                .Take(8)
                .ToListAsync();
        }

        public async Task<Blog> UpdateBlog(Blog blog)
        {
            _blogEntity.Update(blog);

            await _context.SaveChangesAsync();

            return await GetBlog(blog.Id);
        }
    }
}
