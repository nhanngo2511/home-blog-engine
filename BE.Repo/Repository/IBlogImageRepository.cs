﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BE.Repo.Repository
{
    public interface IBlogImageRepository
    {
        Task<IEnumerable<BlogImage>> GetImageByBlogID(int blogID);
        Task CreateBlogImage(string imageName);
        Task AssignImageToBlog(List<string> imageNames, int blogId);
        Task DeleteImagesNotInBlog(List<string> imageNames);
        Task<List<string>> GetImageNameNotInBlog();
    }
}
