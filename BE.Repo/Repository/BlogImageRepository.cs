﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE.Repo.Repository
{
    public class BlogImageRepository : IBlogImageRepository
    {
        private readonly ApplicationContext _context;
        private readonly DbSet<BlogImage> _blogImageEntity;

        public BlogImageRepository(ApplicationContext context)
        {
            this._context = context;
            _blogImageEntity = context.Set<BlogImage>();
        }

        public async Task AssignImageToBlog(List<string> imageNames, int blogId)
        {
            List<BlogImage> blogImages = new List<BlogImage>();

            foreach (var imageName in imageNames)
            {
                BlogImage blogimg = await _blogImageEntity.FirstOrDefaultAsync(x => x.FileName == imageName);
                blogimg.BlogId = blogId;

                _context.Entry(blogimg).State = EntityState.Modified;
            }

            await _context.SaveChangesAsync();
        }

        public async Task CreateBlogImage(string imageName)
        {
            var blogImage = new BlogImage();
            blogImage.FileName = imageName;

            await _context.AddAsync(blogImage);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteImagesNotInBlog(List<string> imageNames)
        {
            var blogImages = new List<BlogImage>();

            foreach (string imageName in imageNames)
            {
                BlogImage blogimg = await _blogImageEntity.FirstOrDefaultAsync(x => x.FileName == imageName);

                blogImages.Add(blogimg);
            }

            _blogImageEntity.RemoveRange(blogImages);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<BlogImage>> GetImageByBlogID(int blogID)
        {
            return await _blogImageEntity.Where(b => b.BlogId == blogID).ToListAsync();
        }

        public async Task<List<string>> GetImageNameNotInBlog()
        {
            return await _blogImageEntity.Where(b => b.BlogId == null).Select(b => b.FileName).ToListAsync();
        }
    }
}
