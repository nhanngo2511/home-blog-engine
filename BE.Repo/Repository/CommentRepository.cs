﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace BE.Repo.Repository
{
    public class CommentRepository : ICommentRepository
    {
        private readonly ApplicationContext _context;
        private readonly DbSet<Comment> _commentEntity;

        public CommentRepository(ApplicationContext context)
        {
            this._context = context;
            this._commentEntity = context.Set<Comment>();
        }

        public async Task<Comment> CreateComment(Comment comment)
        {
            await _context.AddAsync(comment);

            await _context.SaveChangesAsync();

            var commentResult = await GetCommentByID(comment.Id);

            return commentResult;
        }

        public async Task<IEnumerable<Comment>> GetCommentsByBlogId(int blogId)
        {
            return await _commentEntity.Where(x => x.Blog.Id == blogId).Include(x => x.Blog).AsNoTracking().ToListAsync();
        }

        public async Task<Comment> GetCommentByID(int Id)
        {
            return await _commentEntity.FindAsync(Id);
        }
    }
}
