﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace BE.Repo.EntityMapBuilder
{
    public class CommentEntityBuilder
    {
        public CommentEntityBuilder(EntityTypeBuilder<Comment> entityBuilder)
        {
            entityBuilder.HasKey(c => c.Id);
            entityBuilder.Property(c => c.Content).IsRequired();
            entityBuilder.Property(b => b.CreatedDate).IsRequired().HasDefaultValueSql("GetDate()");
            entityBuilder.Property(b => b.UpdatedDate).IsRequired().HasDefaultValueSql("GetDate()");

            entityBuilder.HasOne(b => b.Blog).WithMany(b => b.Commnents).HasForeignKey(b => b.BlogId).OnDelete(DeleteBehavior.Restrict);
        }
    }
}
