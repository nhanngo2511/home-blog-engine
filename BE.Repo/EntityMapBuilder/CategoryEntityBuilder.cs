﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace BE.Repo.EntityMapBuilder
{
    public class CategoryEntityBuilder
    {
        public CategoryEntityBuilder(EntityTypeBuilder<Category> entityBuilder)
        {
            entityBuilder.HasKey(b => b.Id);
            entityBuilder.Property(b => b.Content).IsRequired();
            entityBuilder.Property(b => b.CreatedDate).IsRequired().HasDefaultValueSql("GetDate()");
            entityBuilder.Property(b => b.UpdatedDate).IsRequired().HasDefaultValueSql("GetDate()");
        }
    }
}
