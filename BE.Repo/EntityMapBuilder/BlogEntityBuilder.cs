﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace BE.Repo.EntityMapBuilder
{
    public class BlogEntityBuilder
    {
        public BlogEntityBuilder(EntityTypeBuilder<Blog> entityBuilder)
        {

            entityBuilder.HasKey(b => b.Id);
            entityBuilder.Property(b => b.Title).IsRequired();
            entityBuilder.Property(b => b.ThumnailImageName).IsRequired();
            entityBuilder.Property(b => b.Content).IsRequired();
            entityBuilder.Property(b => b.CreatedDate).IsRequired().HasDefaultValueSql("GetDate()");
            entityBuilder.Property(b => b.UpdatedDate).IsRequired().HasDefaultValueSql("GetDate()");

            entityBuilder.HasOne(a => a.Category).WithMany(a => a.Blogs).HasForeignKey(b => b.CategoryId).OnDelete(DeleteBehavior.Restrict);
        }
    }
}
