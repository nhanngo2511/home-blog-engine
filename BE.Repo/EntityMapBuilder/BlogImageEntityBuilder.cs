﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace BE.Repo.EntityMapBuilder
{
    class BlogImageEntityBuilder
    {
        public BlogImageEntityBuilder(EntityTypeBuilder<BlogImage> entityBuilder)
        {
            entityBuilder.HasKey(b => b.Id);
            entityBuilder.Property(b => b.FileName).IsRequired();
            entityBuilder.Property(b => b.CreatedDate).IsRequired().HasDefaultValueSql("GetDate()");
            entityBuilder.Property(b => b.UpdatedDate).IsRequired().HasDefaultValueSql("GetDate()");

            entityBuilder.HasOne(a => a.Blog).WithMany(a => a.BlogImages).HasForeignKey(a => a.BlogId).OnDelete(DeleteBehavior.Restrict);
        }
    }
}
