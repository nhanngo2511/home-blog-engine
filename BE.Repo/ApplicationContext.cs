﻿using BE.Repo.EntityMapBuilder;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BE.Repo
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            new BlogEntityBuilder(modelBuilder.Entity<Blog>());
            new CategoryEntityBuilder(modelBuilder.Entity<Category>());
            new CommentEntityBuilder(modelBuilder.Entity<Comment>());
            new BlogImageEntityBuilder(modelBuilder.Entity<BlogImage>());

        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            var entries = ChangeTracker
                .Entries()
                .Where(e => e.Entity is BaseEntity && (
                        e.State == EntityState.Added
                        || e.State == EntityState.Modified));

            foreach (var entityEntry in entries)
            {
                if (entityEntry.State == EntityState.Deleted)
                {
                    break;
                }

                if (entityEntry.State == EntityState.Modified)
                {
                    entityEntry.Property("CreatedDate").IsModified = false;
                }

                if (entityEntry.State == EntityState.Added)
                {
                    ((BaseEntity)entityEntry.Entity).CreatedDate = DateTime.Now;
                }

                ((BaseEntity)entityEntry.Entity).UpdatedDate = DateTime.Now;
            }

            return base.SaveChangesAsync(cancellationToken);
        }
    }
}
