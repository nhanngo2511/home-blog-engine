﻿using BE.Repo;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BE.Web.Models
{
    public class BlogModel
    {
        public int Id { get; set; }

        [Display(Name = "Title")]
        public string Title { get; set; }

        [Display(Name = "Image")]
        public string ThumnailImageName { get; set; }

        [Display(Name = "Content")]
        public string Content { get; set; }

        [Display(Name = "CreatedDate")]
        public DateTime CreatedDate { get; set; }

        [Display(Name = "UpdatedDate")]
        public DateTime UpdatedDate { get; set; }

        public Category Category { get; set; }

        public IEnumerable<Comment> Commnents { get; set; }

        public IEnumerable<BlogImage> BlogImages { get; set; }

    }
}
