﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BA.Web.Models
{
    public class PostCommentModel
    {
        public int BlogId { get; set; }
        public string Content { get; set; }
    }
}
