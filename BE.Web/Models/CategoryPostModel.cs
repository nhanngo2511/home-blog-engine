﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BE.Web.Models
{
    public class CategoryPostModel
    {
        [Required]
        public string Content { get; set; }
    }
}
