﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BE.Web.Models
{
    public class AccountLoginModel
    {
        public string username { get; set; }
        public string password { get; set; }
    }
}
