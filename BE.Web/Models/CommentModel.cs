﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BE.Web.Models
{
    public class CommentModel
    {
        public int Id { get; set; }
            
        [Required]
        public string Content { get; set; }

        public int BlogId { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
