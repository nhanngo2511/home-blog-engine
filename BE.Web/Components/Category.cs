﻿using AutoMapper;
using BE.Service;
using BE.Web.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BE.Web.Components
{
    public class Category : ViewComponent
    {
        private readonly ICategoryService _categoryService;
        private readonly IMapper _mapper;
        public Category(ICategoryService categoryService, IMapper mapper)
        {
            this._categoryService = categoryService;
            this._mapper = mapper;
        }

        public IViewComponentResult Invoke()
        {
            var categoriesResult = _categoryService.GetLatestCategories().Result;
            var categories = _mapper.Map<IEnumerable<CategoryModel>>(categoriesResult);
            return View(categories);
        }
    }
}
