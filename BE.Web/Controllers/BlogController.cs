﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BE.Repo;
using BE.Service;
using BE.Web.Helpers;
using BE.Web.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.FileProviders;

namespace BE.Web.Controllers
{
    public class BlogController : Controller
    {

        private readonly IBlogService _blogService;
        private readonly IMapper _mapper;

        public BlogController(IBlogService blogService, IMapper mapper)
        {
            this._blogService = blogService;
            this._mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> Index(int? page)
        {
            var blogs = await _blogService.GetLatestBlogs();

            var blogsModel = _mapper.Map<IEnumerable<BlogModel>>(blogs);

            int pageSize = 6;

            return View(await PaginatedList<BlogModel>.CreateAsync(blogsModel, page ?? 1, pageSize));
        }

        public async Task<IActionResult> Details(int id, string slug)
        {
            var blog = await _blogService.GetBlog(id);

            if (blog == null || slug != SlugGenerator.ToSlug(blog.Title))
            {
                return NotFound();
            }

            var blogModel = _mapper.Map<BlogModel>(blog);

            return View(blogModel);
        }

    }
}
