﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BE.Repo;
using BE.Service;
using BE.Web.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BE.Web.Controllers
{
    public class CommentController : Controller
    {

        private readonly ICommentService _commentService;
        private readonly IMapper _mapper;

        public CommentController(ICommentService commentService, IMapper mapper)
        {
            this._commentService = commentService;
            this._mapper = mapper;
        }

        [HttpPost]
        public async Task<JsonResult> Create(CommentModel comment)
        {

            var commentEntity = _mapper.Map<Comment>(comment);

            var commentResult = await _commentService.CreateComment(commentEntity);

            string strDatetime = String.Format("{0:G}", commentResult.CreatedDate);
            commentResult.CreatedDate = DateTime.Parse(strDatetime);

            return Json(new { comment = commentResult, accountName = "Anonymous" });

        }
    }
}
