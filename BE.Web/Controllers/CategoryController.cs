﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BE.Service;
using BE.Web.Helpers;
using BE.Web.Models;
using Microsoft.AspNetCore.Mvc;

namespace BE.Web.Controllers
{
    public class CategoryController : Controller
    {
        private readonly ICategoryService _categoryService;
        private readonly IBlogService _blogService;
        private readonly IMapper _mapper;

        public CategoryController(ICategoryService categoryService, IBlogService _blogService, IMapper mapper)
        {
            this._categoryService = categoryService;
            this._blogService = _blogService;
            this._mapper = mapper;
        }

        public async Task<IActionResult> Index(int id, string slug, int? page = 1)
        {
            if (id <= 0)
            {
                return NotFound();
            }

            var category = await _categoryService.GetCategoryById(id);

            if (slug != SlugGenerator.ToSlug(category.Content) || category == null)
            {
                return NotFound();
            }

            var categoryModel = _mapper.Map<CategoryModel>(category);

            ViewBag.CurrentCategory = categoryModel.Content;

            int pageSize = 6;

            return View(await PaginatedList<BlogModel>.CreateAsync(categoryModel.Blogs, page ?? 1, pageSize));
        }
    }
}
