﻿using BE.Repo;
using BE.Web.Models;
using BE.Web.ApiModels;
using System.Linq;

namespace BE.Web.Automapper
{
    public class AutomapperProfile : AutoMapper.Profile
    {
        public AutomapperProfile()
        {
            // For MVC Models
            CreateMap<BlogModel, Blog>().ReverseMap();
            CreateMap<CategoryModel, Category>().ReverseMap()
                    .ForMember(destination => destination.Blogs, opt => opt.MapFrom(s => s.Blogs));
            CreateMap<CommentModel, Comment>().ReverseMap();
            CreateMap<Blog, BlogPostModel>().ReverseMap();
            CreateMap<CategoryPostModel, Category>().ReverseMap();

            // For API Models

            CreateMap<Blog, ApiBlogModel>()
                //.ForPath(des => des.CreatedDate, opt => opt.MapFrom(s => s.CreatedDate.ToString("dd/MM/yyyy hh:mm:ss")))
                //.ForPath(des => des.UpdatedDate, opt => opt.MapFrom(s => s.UpdatedDate.ToString("dd/MM/yyyy hh:mm:ss")))
                .ForPath(des => des.CategoryName, opt => opt.MapFrom(s => s.Category.Content))
                .ForMember(des => des.TotalCommnents, opt => opt.MapFrom(s => s.Commnents.Count()))
                .ReverseMap();

            CreateMap<Category, ApiCategoryModel>()
                .ForMember(des => des.TotalBlogs, opt => opt.MapFrom(s => s.Blogs.Count()));

            CreateMap<Category, ApiCategoryPostModel>().ReverseMap();
        }
    }
}
