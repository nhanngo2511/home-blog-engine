﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using HtmlAgilityPack;
using System.IO;
using System.Net;

namespace BE.Web.Helpers
{
    public static class HtmlRender
    {


        public static string Truncate(string html, int indexOfString)
        {


            html = WebUtility.HtmlDecode(html);

            html = RemoveHtmlTag(html);

            html = (html.Length > indexOfString) ? html.Substring(0, indexOfString) : html;

            int lastindex = html.LastIndexOf(' ');
            html = (lastindex > 0) ? html.Substring(0, lastindex) + " ..." : html + " ...";

            return html;
        }

        public static string RemoveHtmlTag(string html)
        {
            return Regex.Replace(html, "<.*?>", String.Empty);
        }
    }
}
