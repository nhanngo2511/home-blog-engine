﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BE.Web.Helpers
{
    public static class HtmlAgility
    {
        public static List<string> GetImageNameFromHtml(string html)
        {

            var htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(html);

            string a = htmlDoc.ToString();

            var htmlNodes = htmlDoc.DocumentNode.SelectNodes("p/img");

            List<string> imageNames = new List<string>();

            foreach (var node in htmlNodes)
            {
                int length = node.Attributes["src"].Value.Length;
                string fileName = node.Attributes["src"].Value.Substring(8, length - 8);

                imageNames.Add(fileName);
            }

            return imageNames;

        }
    }
}
