﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace BE.Web.Helpers
{
    public static class ImageBehavior
    {
        public static string GenerateUniqueFilename(IFormFile file)
        {
            string fileName = Path.GetFileNameWithoutExtension(file.FileName);
            string extentionFile = Path.GetExtension(file.FileName);

            return fileName + "-" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + extentionFile;

            //return ContentDispositionHeaderValue.Parse(
            //                file.ContentDisposition).FileName.ToString().Trim('"');
        }

    }
}
