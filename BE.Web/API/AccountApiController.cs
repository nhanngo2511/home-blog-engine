﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using BE.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BE.Web.Api
{

    [Route("api/account")]
    [ApiController]
    public class AccountApiController : Controller
    {
        public const string username = "admin";
        public const string password = "admin";

        // GET: /<controller>/
        [AllowAnonymous]
        [HttpPost("login")]
        public IActionResult Login(AccountLoginModel accountLogin)
        {
            if (accountLogin == null)
            {
                Unauthorized();
            }

            var isValidAdminAccount = IsValidAdminAccount(accountLogin.username, accountLogin.password);

            if (!isValidAdminAccount)
            {
                ModelState.AddModelError("InvalidUser", "Username or Password was wrong.");
                Unauthorized();
                return Ok(ModelState.Values.Select(x => x.Errors));
            }

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes("This is my Blog Engine using VueJs");

            var tokentDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                   new Claim("Adminname", "Admin")
                }),
                Expires = DateTime.UtcNow.AddDays(2),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokentDescriptor);
            var tokenString = tokenHandler.WriteToken(token);

            return Ok(new
            {
                Token = tokenString
            });

        }

        [HttpGet("logout")]
        public IActionResult Logout()
        {
            
            return Ok(new
            {
                AdminName = true
            });

        }

        private bool IsValidAdminAccount(string usernameParam, string passwordParam) {
            return usernameParam == username && passwordParam == password;
        }
    }
}