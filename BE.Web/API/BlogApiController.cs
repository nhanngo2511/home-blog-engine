﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BE.Repo;
using BE.Service;
using BE.Web.ApiModels;
using BE.Web.Helpers;
using BE.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.FileProviders;


// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BE.Web.Api
{
    [Authorize]
    [Route("api/blog")]
    public class BlogApiController : Controller
    {
        private readonly IBlogService _blogService;
        private readonly IFileProvider _fileProvider;
        private readonly IBlogImageService _blogImageService;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IMapper _mapper;

        private const string physicalImageFileUrl = "../BE.Web/wwwroot/images";

        public BlogApiController(IBlogService blogService, IFileProvider fileProvider, IHostingEnvironment hostingEnvironment, IBlogImageService blogImageService, IMapper mapper)
        {
            this._blogService = blogService;
            this._fileProvider = fileProvider;
            this._hostingEnvironment = hostingEnvironment;
            this._blogImageService = blogImageService;
            this._mapper = mapper;
        }

        [HttpPost("uploadfiles")]
        [Produces("application/json")]
        [AllowAnonymous]
        public IActionResult UploadImageBlog(IFormFile file)
        {
            // Get the mime type
            //var mimeType = file.ContentType;

            var extension = Path.GetExtension(file.FileName);

            var fileName = ImageBehavior.GenerateUniqueFilename(file);

            SaveImageFile(file, "", fileName);

            _blogImageService.CreateBlogImage(fileName);

            Hashtable imageUrl = new Hashtable();

            var domainUrl = HttpContext.Request.Host.Value;

            imageUrl.Add("link", domainUrl + "images/" + fileName);

            return Json(imageUrl);
        }

        [HttpPost("search")]
        public async Task<IActionResult> Search([FromBody]ApiSearchParam apiSearchParam)
        {
            var blogs = await _blogService.GetBlogs();

            var totalBlogs = blogs.Count();

            blogs = SearchBlogs(blogs, apiSearchParam);

            if (apiSearchParam.RowsPerPage > 0)
            {
                blogs = await PaginatedList<Blog>.CreateAsync(blogs, apiSearchParam.Page, apiSearchParam.RowsPerPage);
            }

            var blogsModel = _mapper.Map<IEnumerable<ApiBlogModel>>(blogs);

            var host = GetCurrentHost();

            foreach (var blogModel in blogsModel)
            {
                blogModel.ThumnailImageName = host + "/images/" + blogModel.ThumnailImageName;
            }

            return Ok(new {
                Blogs = blogsModel,
                TotalBlogs = totalBlogs
            });

        }

        [HttpGet("get")]
        public async Task<IActionResult> Get(int Id)
        {
            var blog = await _blogService.GetBlog(Id);

            var blogModel = _mapper.Map<ApiBlogModel>(blog);

            return Ok(blogModel);

        }

        [HttpPost("create")]
        [Produces("application/json")]
        public async Task<IActionResult> Create(BlogPostModel blogPost, IFormFile file)
        {

            if (file == null || file.Length == 0)
            {
                ModelState.AddModelError("ImageRequired", "Please select an image");
                return BadRequest(ModelState);
            }
            else
            {

                string fileName = ImageBehavior.GenerateUniqueFilename(file);
                blogPost.ThumnailImageName = fileName;

                SaveImageFile(file, "", fileName);

                var blog = _mapper.Map<Blog>(blogPost);

                var blogResult = await _blogService.CreateBlog(blog);

                return Ok(blogResult.Id);

            }
        }

        [HttpPost("update")]
        [Produces("application/json")]
        public async Task<IActionResult> Update(BlogPostModel blogPost, IFormFile file)
        {
            
            if (file == null || file.Length == 0)
            {
                ModelState.AddModelError("ImageRequired", "Please select an image");
                return BadRequest(ModelState);
            }
            else
            {
                var currentBlog = await _blogService.GetBlog(blogPost.Id);

                string fileName = ImageBehavior.GenerateUniqueFilename(file);

                var blog = _mapper.Map<Blog>(blogPost);
                blog.ThumnailImageName = fileName;

                var blogResult = await _blogService.UpdateBlog(blog);

                if (blogResult != null && (file.FileName != currentBlog.ThumnailImageName))
                {
                    SaveImageFile(file, "", fileName);
                }

                var blogModel = _mapper.Map<ApiBlogModel>(blogResult);

                return Ok(blogModel);
            }
        }

        [HttpGet("delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            if (id < 0)
            {
                return BadRequest();
            }

            try
            {
                await _blogService.DeleteBlog(id);
            }
            catch (Exception)
            {
                return Ok(false);
            }
            return Ok(true);
        }

        private async void SaveImageFile(IFormFile file, string folderNameToSave, string fileName)
        {

            var fileRoute = Path.Combine(physicalImageFileUrl, folderNameToSave);

            string link = Path.Combine(fileRoute, fileName);

            try
            {
                Stream stream;
                stream = new MemoryStream();
                file.CopyTo(stream);
                stream.Position = 0;
                String serverPath = link;

                using (FileStream writerFileStream = System.IO.File.Create(serverPath))
                {
                    await stream.CopyToAsync(writerFileStream);
                    writerFileStream.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private void RemoveImage(List<string> imageNames, string folderNameToSave)
        {
            string webRootPath = _hostingEnvironment.WebRootPath;

            var fileRoute = Path.Combine(webRootPath, folderNameToSave);

            foreach (string imageName in imageNames)
            {
                string link = Path.Combine(fileRoute, imageName);

                try
                {
                    if (System.IO.File.Exists(link))
                    {
                        System.IO.File.Delete(link);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        private string GetCurrentHost()
        {
            var request = HttpContext.Request;
            var host = request.Host.Value;
            var isHttps = request.IsHttps;

            var fullHost = $"{(isHttps ? "https" : "http")}://{host}";

            return fullHost;
        }

        private IEnumerable<Blog> SearchBlogs(IEnumerable<Blog> blogsResult, ApiSearchParam apiSearchParam) {

            switch (apiSearchParam.SortBy)
            {
                case "id":
                    blogsResult = apiSearchParam.Descending ? blogsResult.OrderByDescending(x => x.Id) : blogsResult.OrderBy(x => x.Id);
                    break;
                case "title":
                    blogsResult = apiSearchParam.Descending ? blogsResult.OrderByDescending(x => x.Title) : blogsResult.OrderBy(x => x.Title);
                    break;
                case "createdDate":
                    blogsResult = apiSearchParam.Descending ? blogsResult.OrderByDescending(x => x.CreatedDate) : blogsResult.OrderBy(x => x.CreatedDate);
                    break;
                case "updatedDate":
                    blogsResult = apiSearchParam.Descending ? blogsResult.OrderByDescending(x => x.UpdatedDate) : blogsResult.OrderBy(x => x.UpdatedDate);
                    break;
                case "totalCommnents":
                    blogsResult = apiSearchParam.Descending ? blogsResult.OrderByDescending(x => x.Commnents.Count()) : blogsResult.OrderBy(x => x.Commnents.Count());
                    break;
                default:
                    return blogsResult.ToList();

            }

            return blogsResult.ToList();
        }
    }
}
