﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BE.Repo;
using BE.Service;
using BE.Web.ApiModels;
using BE.Web.Helpers;
using BE.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BE.Web.Api
{
    [Authorize]
    [Route("api/category")]
    [ApiController]
    public class CategoryApiController : Controller
    {
        private readonly ICategoryService _categoryService;
        private readonly IMapper _mapper;

        public CategoryApiController(ICategoryService categoryService, IMapper mapper)
        {
            this._categoryService = categoryService;
            this._mapper = mapper;
        }

        [HttpGet("categories")]
        public async Task<IActionResult> GetCategories()
        {
            var categories = await _categoryService.GetCategories();

            var categoriesResult = _mapper.Map<IEnumerable<ApiCategoryModel>>(categories);

            return Ok(categoriesResult);
        }

        [HttpPost("search")]
        public async Task<IActionResult> Seach([FromBody]ApiSearchParam apiSearchParam)
        {
            var categories = await _categoryService.GetCategories();

            var totalCategories = categories.Count();

            categories = SearchCategories(categories, apiSearchParam);

            if (apiSearchParam.RowsPerPage > 0)
            {
                categories = await PaginatedList<Category>.CreateAsync(categories, apiSearchParam.Page, apiSearchParam.RowsPerPage);
            }

            var categoriesModel = _mapper.Map<IEnumerable<ApiCategoryModel>>(categories);

            return Ok(new
            {
                Categoies = categoriesModel,
                TotalBlogs = totalCategories
            });
        }

        [HttpGet("category")]
        public async Task<IActionResult> Get(int Id)
        {
            var category = await _categoryService.GetCategoryById(Id);

            var categoryResult = _mapper.Map<ApiCategoryModel>(category);

            return Ok(categoryResult);
        }

        [HttpPost("create")]
        public async Task<IActionResult> Create(CategoryPostModel categoryPostModel)
        {
            var category = _mapper.Map<Category>(categoryPostModel);

            var categoryResult = await _categoryService.CreateCategory(category);

            return Ok(categoryResult);
        }

        [HttpPost("update")]
        public async Task<IActionResult> Update(ApiCategoryPostModel categoryModel)
        {

            var category = _mapper.Map<Category>(categoryModel);

            var categoryResult = await _categoryService.UpdateCategory(category);

            return Ok(categoryResult);
        }

        [HttpGet("delete/{id}")]
        public async Task<IActionResult> Delete (int Id)
        {
            await _categoryService.DeleteCategory(Id);

            return Ok(true);
        }

        private IEnumerable<Category> SearchCategories(IEnumerable<Category> categoriesResult, ApiSearchParam apiSearchParam)
        {

            switch (apiSearchParam.SortBy)
            {
                case "id":
                    categoriesResult = apiSearchParam.Descending ? categoriesResult.OrderByDescending(x => x.Id) : categoriesResult.OrderBy(x => x.Id);
                    break;
                case "content":
                    categoriesResult = apiSearchParam.Descending ? categoriesResult.OrderByDescending(x => x.Content) : categoriesResult.OrderBy(x => x.Content);
                    break;
                case "createdDate":
                    categoriesResult = apiSearchParam.Descending ? categoriesResult.OrderByDescending(x => x.CreatedDate) : categoriesResult.OrderBy(x => x.CreatedDate);
                    break;
                case "updatedDate":
                    categoriesResult = apiSearchParam.Descending ? categoriesResult.OrderByDescending(x => x.UpdatedDate) : categoriesResult.OrderBy(x => x.UpdatedDate);
                    break;
                case "totalBlogs":
                    categoriesResult = apiSearchParam.Descending ? categoriesResult.OrderByDescending(x => x.Blogs.Count()) : categoriesResult.OrderBy(x => x.Blogs.Count());
                    break;
                default:
                    return categoriesResult.ToList();

            }

            return categoriesResult.ToList();
        }

    }
}
