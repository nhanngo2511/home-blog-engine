﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BE.Web.ApiModels
{
    public class ApiCategoryModel : ApiBaseModel
    {
        public string Content { get; set; }

        public int TotalBlogs { get; set; }

    }
}
