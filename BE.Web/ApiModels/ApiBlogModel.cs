﻿using BE.Repo;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BE.Web.ApiModels
{
    public class ApiBlogModel : ApiBaseModel
    {

        [Display(Name = "Title")]
        public string Title { get; set; }

        [Display(Name = "Image")]
        public string ThumnailImageName { get; set; }

        [Display(Name = "Content")]
        public string Content { get; set; }

        public int TotalCommnents { get; set; }

        public string CategoryName { get; set; }


    }
}
