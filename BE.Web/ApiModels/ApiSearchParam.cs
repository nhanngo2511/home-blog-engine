﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BE.Web.ApiModels
{
    public class ApiSearchParam
    {
        public int Page { get; set; }

        public int RowsPerPage { get; set; }

        public bool Descending { get; set; }

        public string SortBy { get; set; }
    }
}