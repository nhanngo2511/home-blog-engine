# BlogEngine

This is a simple blog engine built on the ASP.NET Core 2.2 

# Demo

# Repository

- Clone the repository using the command "git@gitlab.com:nhanngo2511/home-blog-engine.git"
- It's also available on Github: https://gitlab.com/nhanngo2511/home-blog-engine.git 

# Features

## For blog owner (Admin):

- Admin Login/Logout
- Manage categories
- Manage post

## For internet users:
- Show lastest posts, categories menu
- View posts by category
- View post details
- View post on home page
- Add comments

# Technologies used

## User page
- ASP.NET Core 2.2
- Entity Framework Core
- ASP.NET Core Identity
- Dependency Injection
- Custom View Component
- Custom Tag Helper
- AutoMapper
- Url Rewrite (slug)
- Unit Testing (XUnit, Moq)
## Admin page
- VueJs
- Vuetify
- Vuex
- Vue-Router
- VueEditor
- Axios
- Json Web Token

# Local environment to run project
## User page
### Pre-conditions
- Install Server: I have to 2 options:
  - Docker:
    - Install Docker Desktop from: https://www.docker.com/products/docker-desktop
    - After Docker Desktop installed, pull image and build container MS SQL Server in Docker by command: ``` docker run -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=P@ssw0rd' -p 1433:1433 -d mcr.microsoft.com/mssql/server:2017-latest ```
    - Run ``` docker ps ``` to check container was built

  - MS SQL Server
    - Install MS SQLServer 2017 from: https://www.microsoft.com/en-us/sql-server/sql-server-downloads
    - Noted username and password to access to database by ConnectionString

- Install Visual Studio 2019 from: https://www.microsoft.com/en-us/sql-server/sql-server-downloads
- Open BlogEngine.sln and wait for Visual restore all Nuget Packages
- Open BlogEngine.sln and wait for VS restore all Nuget Packages
- Right click on BE.Web project and choose "Set as Startup Project"
- If you build MS SQLServer by Docker then you don't need set up ConnectionString appsetting file, but if you install MS SQLServer then you must to set up ConnectionString appsetting file
- In Package Manager Console -> Target BE.Repo -> Let's input a command: update-database -> press Enter
- Run it

## Admin page
### Pre-conditions
  - Install Visual Code from: https://code.visualstudio.com/download
  - Install NodeJS verson 12.3.1 from: https://nodejs.org/en/download/
  - Go to BE.Admin/blog-engine-admin and open this by Visual Code

- Open terminal window and type ``` npm install ``` -> ``` npm run serve ```
- In login page, just input username: admin, password: admin





