﻿using AutoMapper;
using BE.Repo;
using BE.Repo.Repository;
using BE.Service;
using BE.UnitTest.Helpers;
using BE.Web.Controllers;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace BE.UnitTest.Controllers
{
    public class BlogControllerTest
    {

        private readonly Mock<IBlogRepository> _mockBlogRepo;
        private readonly Mock<IMapper> _mockMapper;
        private readonly IBlogService _blogService;
        private readonly BlogController _blogController;

        public BlogControllerTest()
        {
            _mockBlogRepo = new Mock<IBlogRepository>();
            _mockMapper = new Mock<IMapper>();
            _blogService = new BlogService(_mockBlogRepo.Object);

            _blogController = new BlogController(_blogService, _mockMapper.Object);
        }

        [Fact]
        public void Index_ReturnBlogsToView_Success()
        {
            //Arrange
            var blogs = GenerateDataRepo.GenerateBlogs() as IEnumerable<Blog>;
            _mockBlogRepo.Setup(x => x.GetBlogs()).Returns(Task.FromResult(blogs));

            //Action
            var result = _blogController.Index(1);

            //Assert
            Assert.NotNull(result);
        }

        [Fact]
        public void Details_ReturnBlogToDetailView_Success()
        {
            //Arrange
            var blogs = GenerateDataRepo.GenerateBlogs() as IEnumerable<Blog>;
            _mockBlogRepo.Setup(x => x.GetBlog(It.IsAny<int>())).Returns(Task.FromResult(blogs.First()));

            var slug = "title";

            //Action
            var result = _blogController.Details(1, slug);

            //Assert
            Assert.NotNull(result);
        }

    }
}
