﻿using AutoMapper;
using BE.Repo;
using BE.Repo.Repository;
using BE.Service;
using BE.UnitTest.Helpers;
using BE.Web.Controllers;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BE.UnitTest.Controllers
{
    public class CategoryControllerTest
    {
        private readonly Mock<ICategoryRepository> _mockCategoryRepo;
        private readonly Mock<IMapper> _mockMapper;
        private readonly ICategoryService _categoryService;
        private readonly IBlogService _blogService;
        private readonly CategoryController _categoryController;
        private readonly Mock<IBlogRepository> _mockBlogRepo;


        public CategoryControllerTest()
        {
            _mockCategoryRepo = new Mock<ICategoryRepository>();
            _mockMapper = new Mock<IMapper>();
            _mockBlogRepo = new Mock<IBlogRepository>();
            _blogService = new BlogService(_mockBlogRepo.Object);
            _categoryService = new CategoryService(_mockCategoryRepo.Object);

            _categoryController = new CategoryController(_categoryService, _blogService, _mockMapper.Object);
        }

        [Fact]
        public void Index_ReturnCategories_Success()
        {
            //Arrange
            var categories = GenerateDataRepo.GenerateCategories() as IEnumerable<Category>;
            _mockCategoryRepo.Setup(x => x.GetCategories()).Returns(Task.FromResult(categories));

            var blogs = GenerateDataRepo.GenerateBlogs() as IEnumerable<Blog>;
            _mockBlogRepo.Setup(x => x.GetBlogs()).Returns(Task.FromResult(blogs));

            var slug = "content-test";

            //Action
            var result = _categoryController.Index(1, slug, 1);

            //Asert
            Assert.NotNull(result);
        }
    }
}
