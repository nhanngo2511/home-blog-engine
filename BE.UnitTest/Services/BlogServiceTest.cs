﻿using BE.Repo;
using BE.Repo.Repository;
using BE.Service;
using BE.UnitTest.Helpers;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;


namespace BE.UnitTest.Services
{
    public class BlogServiceTest
    {
        private readonly Mock<IBlogRepository> _mockBlogRepo;
        private readonly BlogService _blogService;

        public BlogServiceTest()
        {
            _mockBlogRepo = new Mock<IBlogRepository>();
            _blogService = new BlogService(_mockBlogRepo.Object);
        }

        [Fact]
        public async void GetBlogs_ReturnBlogs_Success()
        {
            //Arrange
            var blogs = GenerateDataRepo.GenerateBlogs() as IEnumerable<Blog>;
            _mockBlogRepo.Setup(x => x.GetBlogs()).Returns(Task.FromResult(blogs));

            //Action
            var result = await _blogService.GetBlogs();

            //Assert
            Assert.NotNull(result);
        }

        [Fact]
        public async void GetBlog_ReturnBlog_Success()
        {
            //Arrange
            var blogs = GenerateDataRepo.GenerateBlogs() as IEnumerable<Blog>;
            _mockBlogRepo.Setup(x => x.GetBlog(It.IsAny<int>())).Returns(Task.FromResult(blogs.First()));

            //Action
            var result = await _blogService.GetBlog(1);

            //Assert
            Assert.NotNull(result);
        }


    }
}
