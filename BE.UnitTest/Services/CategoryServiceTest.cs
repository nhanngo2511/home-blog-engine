﻿using BE.Repo;
using BE.Repo.Repository;
using BE.Service;
using BE.UnitTest.Helpers;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BE.UnitTest.Services
{
    public class CategoryServiceTest
    {
        private readonly Mock<ICategoryRepository> _mockCategoryRepo;
        private readonly ICategoryService _categoryService;

        public CategoryServiceTest()
        {
            _mockCategoryRepo = new Mock<ICategoryRepository>();
            _categoryService = new CategoryService(_mockCategoryRepo.Object);
        }

        [Fact]
        public void GetCategories_ReturnCategories_Success()
        {
            //Arrange
            var categories = GenerateDataRepo.GenerateCategories() as IEnumerable<Category>;
            _mockCategoryRepo.Setup(x => x.GetCategories()).Returns(Task.FromResult(categories));

            //Action
            var result = _categoryService.GetCategories();

            //Assert
            Assert.NotNull(result);
        }

        [Fact]
        public void GetCategory_ReturnCategory_Success()
        {
            //Arrange
            var categories = GenerateDataRepo.GenerateCategories() as IEnumerable<Category>;
            _mockCategoryRepo.Setup(x => x.GetCategoryById(It.IsAny<int>())).Returns(Task.FromResult(categories.First()));

            //Action
            var result = _categoryService.GetCategoryById(1);

            //Assert
            Assert.NotNull(result);
        }

        [Fact]
        public void GetLastesCategories_ReturnCategories_Success()
        {
            //Arrange
            var categories = GenerateDataRepo.GenerateCategories() as IEnumerable<Category>;
            _mockCategoryRepo.Setup(x => x.GetCategories()).Returns(Task.FromResult(categories));

            //Action
            var result = _categoryService.GetCategoryById(1);

            //Assert
            Assert.NotNull(result);
        }
    }
}
