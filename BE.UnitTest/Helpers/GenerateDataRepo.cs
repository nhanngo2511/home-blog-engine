﻿using BE.Repo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE.UnitTest.Helpers
{
    public static class GenerateDataRepo
    {
        public static List<Category> GenerateCategories()
        {
            return new List<Category>
            {
                new Category
                {
                    Blogs = new List<Blog> {
                        new Blog
                        {
                            Id = 1,
                            CategoryId = 1,
                            Content = "Content",
                            CreatedDate = DateTime.Now,
                            UpdatedDate = DateTime.Now,
                            Title = "Title"
                        },
                        new Blog
                        {
                            Id = 2,
                            CategoryId = 1,
                            Content = "Content",
                            CreatedDate = DateTime.Now,
                            UpdatedDate = DateTime.Now,
                            Title = "Title"
                        }
                    },
                    Content = "Content Test",
                    Id = 1,
                    CreatedDate = DateTime.Now,
                    UpdatedDate = DateTime.Now
                },
                new Category
                {
                    Blogs =  new List<Blog> {
                        new Blog
                        {
                            Id = 3,
                            CategoryId = 2,
                            Content = "Content",
                            CreatedDate = DateTime.Now,
                            UpdatedDate = DateTime.Now,
                            Title = "Title"
                        },
                        new Blog
                        {
                            Id = 4,
                            CategoryId = 2,
                            Content = "Content",
                            CreatedDate = DateTime.Now,
                            UpdatedDate = DateTime.Now,
                            Title = "Title"
                        }
                    },
                    Content = "Content Test",
                    Id = 2,
                    CreatedDate = DateTime.Now,
                    UpdatedDate = DateTime.Now
                },
                new Category
                {
                    Blogs =  new List<Blog> {
                        new Blog
                        {
                            Id = 5,
                            CategoryId = 3,
                            Content = "Content",
                            CreatedDate = DateTime.Now,
                            UpdatedDate = DateTime.Now,
                            Title = "Title"
                        },
                        new Blog
                        {
                            Id = 6,
                            CategoryId = 3,
                            Content = "Content",
                            CreatedDate = DateTime.Now,
                            UpdatedDate = DateTime.Now,
                            Title = "Title"
                        }
                    },
                    Content = "Content Test",
                    Id = 3,
                    CreatedDate = DateTime.Now,
                    UpdatedDate = DateTime.Now
                }
            };
        }

        public static List<Blog> GenerateBlogs()
        {
            return new List<Blog>
            {
                new Blog
                {
                    Id = 1,
                    Category = new Category
                    {
                        Blogs =  new List<Blog> {
                            new Blog
                            {
                                Id = 5,
                                CategoryId = 3,
                                Content = "Content",
                                CreatedDate = DateTime.Now,
                                UpdatedDate = DateTime.Now,
                                Title = "Title"
                            },
                            new Blog
                            {
                                Id = 6,
                                CategoryId = 3,
                                Content = "Content",
                                CreatedDate = DateTime.Now,
                                UpdatedDate = DateTime.Now,
                                Title = "Title"
                            }
                        },
                        Content = "Content Test",
                        Id = 3,
                        CreatedDate = DateTime.Now,
                        UpdatedDate = DateTime.Now
                    },
                    BlogImages = new List<BlogImage>{},
                    CategoryId = 3,
                    Commnents = new List<Comment>
                    {
                        new Comment
                        {
                            Blog = new Blog
                            {
                                Id = 6,
                                CategoryId = 3,
                                Content = "Content",
                                CreatedDate = DateTime.Now,
                                UpdatedDate = DateTime.Now,
                                Title = "Title"
                            },
                            BlogId = 1,
                            Id = 1,
                            Content = "Content Test",
                            CreatedDate = DateTime.Now,
                            UpdatedDate = DateTime.Now
                        },
                        new Comment
                        {
                            Blog = new Blog
                            {
                                Id = 6,
                                CategoryId = 3,
                                Content = "Content",
                                CreatedDate = DateTime.Now,
                                UpdatedDate = DateTime.Now,
                                Title = "Title"
                            },
                            BlogId = 1,
                            Id = 2,
                            Content = "Content Test",
                            CreatedDate = DateTime.Now,
                            UpdatedDate = DateTime.Now
                        }
                    },
                    Content = "Content Test",
                    ThumnailImageName = "ThumnailImageName Test",
                    Title = "Title Test",
                    CreatedDate = DateTime.Now,
                    UpdatedDate = DateTime.Now
                },
                new Blog
                {
                    Id = 2,
                    Category =  new Category
                    {
                        Blogs =  new List<Blog> {
                            new Blog
                            {
                                Id = 5,
                                CategoryId = 3,
                                Content = "Content",
                                CreatedDate = DateTime.Now,
                                UpdatedDate = DateTime.Now,
                                Title = "Title"
                            },
                            new Blog
                            {
                                Id = 6,
                                CategoryId = 3,
                                Content = "Content",
                                CreatedDate = DateTime.Now,
                                UpdatedDate = DateTime.Now,
                                Title = "Title"
                            }
                        },
                        Content = "Content Test",
                        Id = 2,
                        CreatedDate = DateTime.Now,
                        UpdatedDate = DateTime.Now
                    },
                    BlogImages = new List<BlogImage>{},
                    CategoryId = 2,
                    Commnents = new List<Comment>
                    {
                        new Comment
                        {
                            Blog = new Blog
                            {
                                Id = 6,
                                CategoryId = 3,
                                Content = "Content",
                                CreatedDate = DateTime.Now,
                                UpdatedDate = DateTime.Now,
                                Title = "Title"
                            },
                            BlogId = 2,
                            Id = 1,
                            Content = "Content Test",
                            CreatedDate = DateTime.Now,
                            UpdatedDate = DateTime.Now
                        },
                        new Comment
                        {
                            Blog = new Blog
                            {
                                Id = 6,
                                CategoryId = 3,
                                Content = "Content",
                                CreatedDate = DateTime.Now,
                                UpdatedDate = DateTime.Now,
                                Title = "Title"
                            },
                            BlogId = 2,
                            Id = 2,
                            Content = "Content Test",
                            CreatedDate = DateTime.Now,
                            UpdatedDate = DateTime.Now
                        }
                    },
                    Content = "Content Test",
                    ThumnailImageName = "ThumnailImageName Test",
                    Title = "Title Test",
                    CreatedDate = DateTime.Now,
                    UpdatedDate = DateTime.Now
                }
            };
        }

        public static List<Comment> GenerateComments()
        {
            return new List<Comment>
            {
                new Comment
                {
                    Blog = GenerateBlogs().First(),
                    BlogId = GenerateBlogs().First().Id,
                    Id = 1,
                    Content = "Content Test",
                    CreatedDate = DateTime.Now,
                    UpdatedDate = DateTime.Now
                },
                new Comment
                {
                    Blog = GenerateBlogs().First(),
                    BlogId = GenerateBlogs().First().Id,
                    Id = 2,
                    Content = "Content Test",
                    CreatedDate = DateTime.Now,
                    UpdatedDate = DateTime.Now
                }
            };
        }
    }
}
